## Método de la ingeniería para la solución de un problema con el manejo de Gmaps

Yimar David Tamayo

Juan José Valencia

Universidad Icesi

Cali, Valle del cauca

2020

_________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________

*Tabla de contenidos.*

[TOC]

____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________

## 1. Definición del problema

Con la globalización moderna la población se encuentra cada vez más conectada, ejemplo de una de estas formas de conexión son los aeropuertos que han facilitado el transporte y la forma de poder alcanzar múltiples destinos en diversos lugares del mundo, por lo que ha convertido los aviones en un medio de transporte popular y bastante concurrido.  ***TranStats*** (*Transportation Statistics*) es un sitio web que provee acceso a más de 100 bases de datos relacionadas al transporte, gestionado por la oficina de estadística de transporte que hace parte del departamento de transporte de los Estados Unidos, la información que presenta se va recopilando en tiempo real y ofrece de manera detallada datos de interés relacionados al transporte interno del país como horarios , fechas, lugares, entre otros. 

***Flightrends*** es una empresa del sector privado que ha considerado la opción de establecer un nuevo aeropuerto en algunas de las zonas estratégicas del país, pero para eso considera pertinente realizar primero diferentes estudios que le permitan tomar decisiones adecuadas en varios aspectos como economía y uso de recursos. Una de las preocupaciones de la empresa es conocer la demanda de vuelos a lo largo del país y poder realizar un análisis considerable de la misma para así poder ofrecer su oferta aérea, es por esto que ha contratado un equipo de expertos en análisis de datos que les permitan dar un informe claro sobre el estado de la situación. Este equipo cuenta con una de las bases de datos de ***TranStats*** para realizar su trabajo pero se han dado cuenta de que la información es bastante extensa y que analizarla por separado además del trabajo extra, requeriría de mucho más tiempo del planeado,  por lo que han decidido que es mucho mejor si la información pudiera segmentarse para presentarse de manera más resumida. 

El equipo de análisis de datos de ***TranStats*** requiere ayuda con el desarrollo de una aplicación que le permita gestionar de manera más sencilla la manipulación de sus datos y poder:

- Tener su información organizada y clasificada, y poder tomar una decisión respecto a la mejor administración de aerolíneas y destinos.

- Visualizar la información de alguna forma para poder tomar una decisión con respecto al mejor sector de ubicación de la infraestructura.

  

## 2. Recopilación de la información necesaria

Una vez definido el problema es importante realizar una identificación de los requerimientos funcionales con los cuales debe cumplir la aplicación a desarrollar, estos son:

- Permitir la filtración de la información por distintos criterios de búsqueda (por fecha (mes y año), por ciudad de origen, por ciudad de destino, por hora de salida) y poder disminuir la cantidad de los datos visualizados, para ser mostrados al usuario los de su interés de forma más agrupada.
- Permitir la visualización de la información a través de mapas, es decir que los vuelos puedan ser marcados como referencias en un mapa presentado por la interfaz gráfica de usuario para facilitar un análisis grafico de los datos. 
- Permitir realizar un resumen de algunos de los datos más relevantes dentro de la información cargada (tiempo promedio de demora de salida/llegada de un vuelo, tiempo promedio de distancia por vuelo, tiempo promedio en aire) .  

**¿Qué es y cómo funciona Gmaps?**

Gmaps es un servidor de aplicaciones de mapas en la web, ofreciendo imágenes desplazables de mapas que permiten la visualización de cualquier ubicación en el mundo [1].

Con Gmaps podemos visualizar el mapa del mundo usando distintos proveedores de mapas como

·     YahooMapProvider

·     GoogleMapProvider 

·     OpenCycleMapProvider



  ![gmap-yahoo](https://lh3.googleusercontent.com/proxy/MpYHUzn3RzXpCIV_SBjpvJzuoN9Zank9WTpqze0IFQ_CgvsCwem6y5kqxrvMB2XF-1woBZqlsnXESvFLAQAMIzhE1xlyCPBJYegj3jDs8etG)

​	[2]

 ![gmap-openstreetmap](https://lh4.googleusercontent.com/proxy/pd0hpfwvF9XkDZpfQiGd_kNjkoKC1B4WC-W37qyaCVwas28X3OcQtIMfr32WoF9Ef_6eHnLqHai3WMREWW4OV7isssGextukc0ah8HMj9ZVoag4_cq61cLk)

También podemos agregar marcadores que son capas que se colocan en la parte superior de el mapa, llamadas "superposiciones"

![gmap-marker](https://lh3.googleusercontent.com/proxy/tfPlYamu3NZCPD0ZUp_vW7lMz2XD3gSU1RUu-V2fbUaLhDm7liOEdwUnfCWfyyITBKP5Vuvsdk7llG6DCfmcH0YtTzpmdGfwvKIklABmceo3WG4MAyHL)     

 Cuando necesitemos delimitar un área podemos crear polígonos en cierta zona del mapa tal como el siguiente ejemplo

![gmap-polygon](https://lh4.googleusercontent.com/proxy/cjYURxE3nbXpas2NSzkkSlW9fFSbfCObx-JZKD3WwNNBi82lkkCmTU1Naccth9koa47hcz7O6U20KcjGT-4ZwSweMyQ-2mq4OE1rKQ0sI7upQmE) 

Cabe destacar que GMap.NET requiere de una conexión a Internet para poder mantener un contacto con el servidor que provee los mapas y poder funcionar correctamente. Sin embargo, si no existe una conexión disponible, aún se puede usar la información almacenada en caché para mostrar los mapas, lo que incurre en una ventaja para ambas situaciones. 



**Oficina de Estadísticas de Transporte y TranStats** [3] [4] [5]

La Oficina de Estadísticas de Transporte, como parte del Departamento de Transporte de los Estados Unidos , proporciona información oportuna, precisa y creíble sobre el sistema de transporte de los Estados Unidos, compila, analiza y hace accesible la información sobre los sistemas de transporte de la nación , y las consecuencias del transporte para la economía, la sociedad y el medio ambiente. 

TranStats es la "Base de datos de transporte intermodal" de BTS que incluye bases de datos y estadísticas de aviación, carretera, marítimo, ferroviario, oleoducto, peatones y más. Los datos incluyen información financiera, de seguridad, de uso y ambiental.

Una de las base de datos más popular incluida en la colección TranStats es la base de datos de desempeño a tiempo de la aerolínea, que incluye el desempeño a tiempo de cada vuelo, aerolínea y aeropuerto en los Estados Unidos.



## 3. Búsqueda de soluciones creativas

Ahora que se tiene la información necesaria para el problema, es pertinente realizar la búsqueda de soluciones creativas que puedan dar solución al problema con las condiciones requeridas. Por lo anterior, a continuación se plantea una técnica de relación forzada para generar ideas.

El objetivo de esta técnica es poder tomar un componente (entidad, producto, etc) relacionado al problema y un componente totalmente ajeno para luego realizar una conexión forzada entre ellos a partir de palabras que desarrollan el componente en sí mismo.

<u>Mapa:</u> Lugar, viaje, plano, países, ciudades, territorio, distancia, grafico.

<u>Ventilador:</u> Instrumento, aire, energía, maquina, frio.

Distancia y aire, nos conducen a la idea de hacer un mapa de rutas con la ayuda de Gmaps que nos permita simular el recorrido de los vuelos y mostrar la distancia del punto de partida al destino para que el equipo pueda visualizarlos de forma interactiva.

Grafico y maquinas, nos conduce a la idea de hacer una grafico con la información de la base datos dándole al equipo los datos necesarios con la que pueda tomar decisiones importantes.

<u>Vuelo</u>: origen, historial, horario, trayecto, destino.

<u>Cama</u>: comodidad, suavidad, descanso, tranquilidad.

Historial y comodidad, nos conducen a la idea de que debemos organizar de forma cómoda todo el historial de vuelos de la base de datos de cierta manera, para que lea sea fácil al usuario leer la información, por ejemplo mostrar una tabla con la información de cada vuelo y mostrar el vuelo en el mapa.



## 4. Transición de la formulación de ideas a los diseños preliminares

Ya que las ideas han sido generadas, es momento de ampliarlas y realizar un análisis sobre los aspectos extra que conllevaría llevarlas a cabo, con el objetivo de poder realizar un descarte de aquellas ideas que no son del todo útiles.

1. **Mapa de rutas:**  esta idea nos permitiría mostrar de forma muy interactiva la información al usuario, ya que además de mostrar el vuelo, también indicaría su trayecto y la información básica que al usuario le sea útil, sin embargo a pesar de que se lograría resumir la información y visualizarse, habría que comprimir mucha información en un solo lugar pues el volumen de los datos es bastante extenso y esto provocaría una confusión en la visualización de los mapas, pero aún no es imposible.
2. **Gráfica de resumen:** esta idea es una forma sencilla de presentar información importante para el usuario ya que se tomaría los datos relevantes de los vuelos y se construiría una gráfica a partir de ellos, sin embargo no sería conveniente pues estaríamos desaprovechando la herramienta de Gmaps la cual nos permitiría mostrar de alguna forma los vuelos y además una vez más el volumen de la información podría llegar a ser complejo de entender para el usuario, por lo tanto se descarta. 
3. **Tabla de resumen e indicador de vuelo:** Esta idea mostraría un vuelo marcado en un mapa provisto por Gmaps, y la información necesaria relacionada estaría a un lado en una tabla para proveer al usuario los datos más relevantes. Cabe resaltar que para reducir la cantidad de marcadores todos aquellos vuelos con un origen/destino similar se agruparían en la tabla junto con un solo marcador que represente todos los datos, lo que reduciría considerablemente el volumen de información presentada, mostrando únicamente lo necesario.



## 5. Evaluación y selección de la mejor solución

Con el objetivo de poder seleccionar la mejor solución posible para el problema que se busca solucionar es considerable realizar un método de evaluación que permita decidir sobre cual es la mejor opción posible para ser desarrollada.

Para realizar la evaluación de ideas, se establecen los siguientes criterios basados en los costos diversos que conllevan aplicar la solución planteada con un puntaje de 1 a 3 de la siguiente manera, baja (El costo es mínimo para lograr la solución), intermedio (El costo es parcial para lograr la solución), alto (El costo es máximo para lograr la solución). Los criterios son los siguientes:

- Costo de implementación del desarrollador:
  1. Bajo
  2. Intermedio
  3. Alto
- Costo de lectura y comprensión para el usuario:
  1. Bajo
  2. Intermedio
  3. Alto
- Cantidad de información mostrada al usuario:
  1. Bajo
  2. Intermedio
  3. Alto

Basado en esta consideración de costos la solución con menos costo será la seleccionado y la mejor a implementar.

Consecuentemente, las respectivas soluciones posibles y los criterios de juicio desarrollados son agrupados en la siguiente tabla para poder llevar a cabo la evaluación.

| Idea                                  | Costo de implementación | Costo de lectura | Cantidad de información mostrada | Total |
| ------------------------------------- | ----------------------- | ---------------- | -------------------------------- | ----- |
| Mapa de rutas                         | 3                       | 2                | 3                                | 8     |
| Gráfica de resumen                    | 1                       | 2                | 2                                | 5     |
| Tabla de resumen e indicador de vuelo | **2**                   | **1**            | **1**                            | **4** |

A partir de la evaluación anterior, la idea con mejor reducción de costo posible es la de presentar una tabla de resumen y un indicador de vuelo al usuario para poder resumir y mostrar la información, y por lo tanto será la que se usará para solucionar el problema definido.



## 6. Preparación de los informes y especificaciones

Finalmente una vez elegida la solución definitiva a ser desarrollada es importante realizar un diseño que permita el direccionamiento de la misma hacia la implementación, para esto se desarrolla el siguiente diagrama de clases:

- El diagrama de clases se puede encontrar adjunto a la carpeta de documentos del proyecto donde se encuentra también el presente informe.

además para poder tener una concepción general de como se ve el diseño anterior en términos reales del problema se desarrolla el siguiente diagrama de objetos:

- El diagrama de objetos se puede encontrar adjunto a la carpeta de documentos del proyecto donde se encuentra también el presente informe.

Ahora como parte del desarrollo lógico del presente informe se realiza una ***síntesis reflexiva*** que permita aportar una perspectiva autoconsciente sobre los autores y su proceso de aprendizaje y trabajo a la hora de escribirlo:

A través del desarrollo del método de la ingeniería se pudo encontrar de una manera estructurada una forma de poder llegar a la solución de un problema dado, poniendo en evidencia que para lograr un buen análisis de un proyecto es necesario seguir una secuencia que permita lograr llegar a algo con sentido.

Para el problema definido, el plantear soluciones fue algo más complejo de lo esperado, pues la consideración de presentar una información de manera resumida lleva consigo muchas maneras de realizarlo, y por lo tanto muchas opciones de las cuales disponer, pero el objetivo era poder conseguir alguna que fuera eficaz (atendiendo las necesidades del cliente), pero también eficiente a la vez (atendiendo a las preocupaciones del desarrollador). Es por eso que se debió analizar la situación planteada para considerar algunos de los factores más importantes (por ejemplo, aquí se consideraron los costos como lo más urgente) que giran en torno a la problemática en si misma y poder clarificar las soluciones posibles. Esto permite considerar que es bueno ver las cosas desde otras perspectivas como los problemas que hay dentro de los mismos problemas para que una vez reconocidos sea más fácil elaborar una solución que asegure lograr resultados esperados con un alto desempeño.



## 7. Implementación

La debida implementación de la aplicación solicitada, con las funciones requeridas y la solución seleccionada se llevarán a cabo en el lenguaje de programación (C#)  con la lista de las siguientes tareas a desarrollar:

- Leer la información del archivo otorgado por el equipo experto de análisis de datos.
- Filtrar la información y visualizarla para el análisis requerido.
- Diseñar una interfaz de usuario gráfica interactiva que permita el uso de todo lo anterior.

La implementación del proyecto se incluye en el siguiente proyecto de GitLab:

https://gitlab.com/backtojuan/flightswithgmaps



## 8. Referencias

1. (2020, Ene 29). Google maps [En línea]. Disponible en: https://es.wikipedia.org/wiki/Google_Map

2. (2013, Feb 21). MAPS, MARKERS AND POLYGONS [En línea]. Disponible en: http://bit.ly/2SeBPio

3. (2019, Feb 26). About the Bureau of Transportation Statistics [En línea]. Disponible en: http://bit.ly/2RMZqI6

4. (2019, Oct 4). Bureau of Transportation Statistics [En línea]. Disponible en: http://bit.ly/2OhR4pS

5. (2019, Nov). Reporting Carrier On-Time Performance [En línea]. Disponible en: http://bit.ly/2OlqpIG