﻿using System;
using model;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gui
{
    public partial class FlightsWithGmaps : Form
    {

        private Airport airport;

        public FlightsWithGmaps()
        {
            InitializeComponent();
            initializeListView();
            airport = new Airport();
        }

        private void gmap_Load(object sender, EventArgs e)
        {
            gmap.MapProvider = GMap.NET.MapProviders.BingMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerOnly;
            gmap.Position = new GMap.NET.PointLatLng(48.8589507, 2.2775175);
        }

        private void initializeListView() 
        {
            flightsview.View = View.Details;
            flightsview.Columns.Add("FlightDate");
            flightsview.Columns.Add("AirlineID");
            flightsview.Columns.Add("OriginCityName");
            flightsview.Columns.Add("OriginStateName");
            flightsview.Columns.Add("DestCityName");
            flightsview.Columns.Add("DestStateName");
            flightsview.Columns.Add("DepTime");
            flightsview.Columns.Add("DepDelayMinutes");
            flightsview.Columns.Add("ArrTime");
            flightsview.Columns.Add("ArrDelayMinutes");
            flightsview.Columns.Add("Cancelled");
            flightsview.Columns.Add("AirTime");
            flightsview.Columns.Add("Distance");
        }

        private void bttnFilter_Click(object sender, EventArgs e)
        {
            if (cboxFilters.Text.Equals("Month")) 
            {
                List<Flight> searched = airport.searchByMonth(txtQuerie.Text);
                foreach (Flight flight in searched)
                { 
                    flightsview.Items.Add(new ListViewItem(new String[] {flight.getFlightDate(), "" + flight.getAirlineID(),
                    flight.getOriginCityName(), flight.getOriginStateName(), flight.getDestCityName(), flight.getDestStateName()
                    ,"" + flight.getDepTime(), "" + flight.getDepDelayMinutes(), flight.getArrTime(), ""+ flight.getArrDelayMinutes()
                    ,""+flight.getCancelled(), ""+flight.getAirTime(),""+flight.getDistance()}));
                }
            }
            else if (cboxFilters.Text.Equals("Year"))
            {
                List<Flight> searched = airport.searchByYear(txtQuerie.Text);
                foreach (Flight flight in searched)
                {
                    flightsview.Items.Add(new ListViewItem(new String[] {flight.getFlightDate(), "" + flight.getAirlineID(),
                    flight.getOriginCityName(), flight.getOriginStateName(), flight.getDestCityName(), flight.getDestStateName()
                    ,"" + flight.getDepTime(), "" + flight.getDepDelayMinutes(), flight.getArrTime(), ""+ flight.getArrDelayMinutes()
                    ,""+flight.getCancelled(), ""+flight.getAirTime(),""+flight.getDistance()}));
                }
            }
            else if (cboxFilters.Text.Equals("OriginState"))
            {
                List<Flight> searched = airport.searchByOriginState(txtQuerie.Text);
                foreach (Flight flight in searched)
                {
                    flightsview.Items.Add(new ListViewItem(new String[] {flight.getFlightDate(), "" + flight.getAirlineID(),
                    flight.getOriginCityName(), flight.getOriginStateName(), flight.getDestCityName(), flight.getDestStateName()
                    ,"" + flight.getDepTime(), "" + flight.getDepDelayMinutes(), flight.getArrTime(), ""+ flight.getArrDelayMinutes()
                    ,""+flight.getCancelled(), ""+flight.getAirTime(),""+flight.getDistance()}));
                }
            }
            else if (cboxFilters.Text.Equals("DestinyState"))
            {
                List<Flight> searched = airport.searchByDestState(txtQuerie.Text);
                foreach (Flight flight in searched)
                {
                    flightsview.Items.Add(new ListViewItem(new String[] {flight.getFlightDate(), "" + flight.getAirlineID(),
                    flight.getOriginCityName(), flight.getOriginStateName(), flight.getDestCityName(), flight.getDestStateName()
                    ,"" + flight.getDepTime(), "" + flight.getDepDelayMinutes(), flight.getArrTime(), ""+ flight.getArrDelayMinutes()
                    ,""+flight.getCancelled(), ""+flight.getAirTime(),""+flight.getDistance()}));
                }
            }
            else if (cboxFilters.Text.Equals("DepartureTime"))
            { 
                List<Flight> searched = airport.searchByDepTime(txtQuerie.Text);
                foreach (Flight flight in searched)
                {
                    flightsview.Items.Add(new ListViewItem(new String[] {flight.getFlightDate(), "" + flight.getAirlineID(),
                    flight.getOriginCityName(), flight.getOriginStateName(), flight.getDestCityName(), flight.getDestStateName()
                    ,"" + flight.getDepTime(), "" + flight.getDepDelayMinutes(), flight.getArrTime(), ""+ flight.getArrDelayMinutes()
                    ,""+flight.getCancelled(), ""+flight.getAirTime(),""+flight.getDistance()}));
                }
            }
        }
    }
}
