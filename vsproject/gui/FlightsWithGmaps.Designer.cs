﻿namespace gui
{
    partial class FlightsWithGmaps
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlightsWithGmaps));
            this.gmap = new GMap.NET.WindowsForms.GMapControl();
            this.flightsview = new System.Windows.Forms.ListView();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboxFilters = new System.Windows.Forms.ComboBox();
            this.bttnFilter = new System.Windows.Forms.Button();
            this.txtQuerie = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // gmap
            // 
            this.gmap.Bearing = 0F;
            this.gmap.CanDragMap = true;
            this.gmap.EmptyTileColor = System.Drawing.Color.Maroon;
            this.gmap.GrayScaleMode = false;
            this.gmap.HelperLineOption = GMap.NET.WindowsForms.HelperLineOptions.DontShow;
            this.gmap.LevelsKeepInMemmory = 5;
            this.gmap.Location = new System.Drawing.Point(14, 93);
            this.gmap.MarkersEnabled = true;
            this.gmap.MaxZoom = 20;
            this.gmap.MinZoom = 5;
            this.gmap.MouseWheelZoomEnabled = true;
            this.gmap.MouseWheelZoomType = GMap.NET.MouseWheelZoomType.MousePositionAndCenter;
            this.gmap.Name = "gmap";
            this.gmap.NegativeMode = false;
            this.gmap.PolygonsEnabled = true;
            this.gmap.RetryLoadTile = 0;
            this.gmap.RoutesEnabled = true;
            this.gmap.ScaleMode = GMap.NET.WindowsForms.ScaleModes.Integer;
            this.gmap.SelectedAreaFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(65)))), ((int)(((byte)(105)))), ((int)(((byte)(225)))));
            this.gmap.ShowTileGridLines = false;
            this.gmap.Size = new System.Drawing.Size(560, 385);
            this.gmap.TabIndex = 0;
            this.gmap.Zoom = 5D;
            this.gmap.Load += new System.EventHandler(this.gmap_Load);
            // 
            // flightsview
            // 
            this.flightsview.FullRowSelect = true;
            this.flightsview.GridLines = true;
            this.flightsview.HideSelection = false;
            this.flightsview.Location = new System.Drawing.Point(628, 93);
            this.flightsview.Name = "flightsview";
            this.flightsview.Size = new System.Drawing.Size(487, 385);
            this.flightsview.TabIndex = 1;
            this.flightsview.UseCompatibleStateImageBehavior = false;
            this.flightsview.View = System.Windows.Forms.View.Details;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(48, 20);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(59, 53);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(142, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 55);
            this.label1.TabIndex = 3;
            this.label1.Text = "FLIGHTRENDS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(624, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Filter by:";
            // 
            // cboxFilters
            // 
            this.cboxFilters.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboxFilters.FormattingEnabled = true;
            this.cboxFilters.Items.AddRange(new object[] {
            "Month",
            "Year",
            "OriginState",
            "DestinyState",
            "DepartureTime"});
            this.cboxFilters.Location = new System.Drawing.Point(708, 33);
            this.cboxFilters.Name = "cboxFilters";
            this.cboxFilters.Size = new System.Drawing.Size(121, 28);
            this.cboxFilters.TabIndex = 5;
            // 
            // bttnFilter
            // 
            this.bttnFilter.ForeColor = System.Drawing.Color.Black;
            this.bttnFilter.Location = new System.Drawing.Point(1000, 28);
            this.bttnFilter.Name = "bttnFilter";
            this.bttnFilter.Size = new System.Drawing.Size(106, 41);
            this.bttnFilter.TabIndex = 6;
            this.bttnFilter.Text = "Filter";
            this.bttnFilter.UseVisualStyleBackColor = true;
            this.bttnFilter.Click += new System.EventHandler(this.bttnFilter_Click);
            // 
            // txtQuerie
            // 
            this.txtQuerie.Location = new System.Drawing.Point(860, 39);
            this.txtQuerie.Name = "txtQuerie";
            this.txtQuerie.Size = new System.Drawing.Size(98, 20);
            this.txtQuerie.TabIndex = 7;
            // 
            // FlightsWithGmaps
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(1148, 489);
            this.Controls.Add(this.txtQuerie);
            this.Controls.Add(this.bttnFilter);
            this.Controls.Add(this.cboxFilters);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.flightsview);
            this.Controls.Add(this.gmap);
            this.Font = new System.Drawing.Font("Arial Rounded MT Bold", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FlightsWithGmaps";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FlightsWithGmaps";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GMap.NET.WindowsForms.GMapControl gmap;
        private System.Windows.Forms.ListView flightsview;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboxFilters;
        private System.Windows.Forms.Button bttnFilter;
        private System.Windows.Forms.TextBox txtQuerie;
    }
}

