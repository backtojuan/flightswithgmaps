﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    public class Flight
    {
        private String flightDate;
        private int airlineID;
        private String originCityName;
        private String originStateName;
        private String destCityName;
        private String destStateName;
        private String depTime;
        private double depDelayMinutes;
        private String arrTime;
        private double arrDelayMinutes;
        private double cancelled;
        private double airTime;
        private double distance;

        public Flight(String flightDate, int airlineID, String originCityName, String originStateName, String destCityName,
            String destStateName, String depTime, double depDelayMinutes, String arrTime, double arrDelayMinutes, 
            double cancelled, double airTime, double distance)
        {
            this.flightDate = flightDate;
            this.airlineID = airlineID;
            this.originCityName = originCityName;
            this.originStateName = originStateName;
            this.destCityName = destCityName;
            this.destStateName = destStateName;
            this.depTime = depTime;
            this.depDelayMinutes = depDelayMinutes;
            this.arrTime = arrTime;
            this.arrDelayMinutes = arrDelayMinutes;
            this.cancelled = cancelled;
            this.airTime = airTime;
            this.distance = distance;
        }

        public String getFlightDate() {
            return flightDate;
        }

        public int getAirlineID()
        {
            return airlineID;
        }

        public String getOriginCityName()
        {
            return originCityName;
        }

        public String getOriginStateName()
        {
            return originStateName;
        }

        public String getDestCityName() 
        {
            return destCityName;
        }
        
        public String getDestStateName()
        {
            return destStateName;
        }
        
        public String getDepTime()
        {
            return depTime;
        }

        public double getDepDelayMinutes()
        {
            return depDelayMinutes;
        }

        public String getArrTime() 
        {
            return arrTime; 
        }

        public double getArrDelayMinutes() 
        {
            return arrDelayMinutes;
        }

        public double getCancelled() 
        {
            return cancelled;
        }

        public double getAirTime() 
        {
            return airTime;
        }

        public double getDistance() 
        {
            return distance;
        }
    }
}
