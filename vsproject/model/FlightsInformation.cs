﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace model
{
    public class FlightsInformation
    {
        private List<Flight> flights;
        public const String PATH = "..\\..\\..\\data\\flights.csv";

<<<<<<< HEAD
        public List<Flight> load()
=======

        public void load()
>>>>>>> 2ef7018b979d0ba765dad5fc06b2b721970812ac
        {
            flights = new List<Flight>();
            String line;
            StreamReader str = new StreamReader(PATH);
            line = str.ReadLine();
            
            try
            {
                while (line != null) 
                {
                    line = str.ReadLine();
                    String date = "";
                    int airlineId = 0;
                    String originCityName = "";
                    String originStateName = "";
                    String destCityName = "";
                    String destStateName = "";
                    String depTime = "";
                    double depDelayMinutes = 0.0;
                    String arrTime = "";
                    double arrDelayMinutes = 0.0;
                    double cancelled = 0.0;
                    double airtime = 0.0;
                    double distance = 0.0;

                    String[] args = line.Split(';');

                    if (args[0] != "")
                    {
                        date = args[0];
                    }
                    if (args[1] != "") 
                    {
                        airlineId = int.Parse(args[1]);
                    }
                    if (args[2] != "") 
                    {
                        originCityName = args[2];
                    }
                    if(args[3] != "") 
                    {
                        originStateName = args[3];
                    }
                    if (args[4] != "") 
                    {
                        destCityName = args[4];
                    }
                    if (args[5] != "")
                    {
                        destStateName = args[5];
                    }
                    if (args[6] != "")
                    {
                       depTime = args[6];
                    }
                    if (args[7] != "")
                    {
                        depDelayMinutes = Double.Parse(args[7]);
                    }
                    if (args[8] != "")
                    {
                        arrTime = args[8];
                    }
                    if (args[9] != "")
                    {
                        arrDelayMinutes = Double.Parse(args[9]);
                    }
                    if (args[10] != "")
                    {
                        cancelled = Double.Parse(args[10]);
                    }
                    if (args[11] != "")
                    {
                        airtime = Double.Parse(args[11]);
                    }
                    if (args[12] != "") 
                    {
                        distance = Double.Parse(args[12]);
                    }
                    line = str.ReadLine();

                    Flight flight = new Flight(date, airlineId, originCityName, originStateName, destCityName, destStateName, depTime,
                    depDelayMinutes, arrTime, arrDelayMinutes, cancelled, airtime, distance);

                    flights.Add(flight);
                }
            }
            catch (Exception e) 
            {
                Console.WriteLine("Exception" + e.Message);
            }

            return flights;
        }
    }
}