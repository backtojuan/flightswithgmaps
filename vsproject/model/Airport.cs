﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace model
{
    public class Airport
    {

        private List<Flight> flights;
        private FlightsInformation fi;

        public Airport() 
        {
            this.fi = new FlightsInformation();
            flights = fi.load();
        }

        public List<Flight> searchByMonth(String month) 
        {
            List<Flight> searched = new List<Flight>();
         
            for (int i = 0; i < flights.Count(); i++) 
            {
                Boolean m = flights.ElementAt(i).getFlightDate().Contains(month);
                if (m == true) 
                {
                    searched.Add(flights.ElementAt(i));      
                }
            }
            return searched;
        }


        public List<Flight> searchByYear(String year)
        {
            List<Flight> searched = new List<Flight>();

            for (int i = 0; i < flights.Count(); i++)
            {
                Boolean m = flights.ElementAt(i).getFlightDate().Contains(year);
                if (m == true)
                {
                    searched.Add(flights.ElementAt(i));
                }
            }
            return searched;
        }

        public List<Flight> searchByOriginState(String originState)
        {
            List<Flight> searched = new List<Flight>();

            for (int i = 0; i < flights.Count(); i++)
            {
                Boolean o = flights.ElementAt(i).getOriginStateName().Contains(originState);
                
                if(o==true)
                {
                    searched.Add(flights.ElementAt(i));
                }
            }
            return searched;
        }

        public List<Flight> searchByDestState(String destState)
        {
            List<Flight> searched = new List<Flight>();

            for (int i = 0; i < flights.Count(); i++)
            {
                Boolean o = flights.ElementAt(i).getDestStateName().Contains(destState);
                if (o == true)
                {
                    searched.Add(flights.ElementAt(i));
                }
            }
            return searched;
        }

        public List<Flight> searchByDepTime(String depTime)
        {
            List<Flight> searched = new List<Flight>();

            for (int i = 0; i < flights.Count(); i++)
            {
                Boolean m = flights.ElementAt(i).getDepTime().Contains(depTime);

                if (m==true)
                {
                    searched.Add(flights.ElementAt(i));
                }
            }
            return searched;
        }
    }
}
